/**
 ******************************************************************************
 * @file    buffered_uart.c
 * @author  nicolas.brunner@heig-vd.ch
 * @date    07-August-2018
 * @brief   Driver for UART using circular buffer
 ******************************************************************************
 * @copyright HEIG-VD
 *
 * License information
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/

#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "radio_config.h"

#include "buffered_uart.h"

/* Private define ------------------------------------------------------------*/

#define RX_BUFFER_SIZE     512
#define TX_BUFFER_SIZE    1024

/* Private variables ---------------------------------------------------------*/

/** Buffer for the RX */
static uint8_t rx_buffer[RX_BUFFER_SIZE];
static uint16_t rx_buffer_read_index;
static uint16_t rx_buffer_fill_count;

/** Buffer for the TX */
static uint8_t tx_buffer[TX_BUFFER_SIZE];
static uint16_t tx_buffer_read_index;
static uint16_t tx_buffer_write_index;
static volatile uint16_t tx_buffer_fill_count;
static uint16_t tx_dma_size;

static UART_HandleTypeDef uart_handle;
static DMA_HandleTypeDef dma_rx_handle;
static DMA_HandleTypeDef dma_tx_handle;

/* Private functions declaration ---------------------------------------------*/

static void update_rx_buffer_fill_count(void);
static void start_tx_dma();

/* Public functions ----------------------------------------------------------*/

void buffered_uart_init(uint32_t baudrate, uint32_t word_length, uint32_t parity, uint32_t flow_control)
{
    HAL_StatusTypeDef status;

    rx_buffer_read_index = 0;
    rx_buffer_fill_count = 0;
    tx_buffer_read_index = 0;
    tx_buffer_write_index = 0;
    tx_buffer_fill_count = 0;

    ISM_UART_CLK_ENABLE();
    ISM_UART_DMA_CLK_ENABLE();

#ifdef STM32L4
    // Use HSI for waking up in stop mode
    RCC_PeriphCLKInitTypeDef PeriphClkInit;
#if ISM_UART_NUMBER == 5
    PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_UART5;
    PeriphClkInit.Uart5ClockSelection = RCC_UART5CLKSOURCE_HSI;
#elif ISM_UART_NUMBER == 4
    PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_UART4;
    PeriphClkInit.Uart4ClockSelection = RCC_UART4CLKSOURCE_HSI;
#elif ISM_UART_NUMBER == 3
    PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART3;
    PeriphClkInit.Usart3ClockSelection = RCC_USART3CLKSOURCE_HSI;
#elif ISM_UART_NUMBER == 2
    PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2;
    PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_HSI;
#elif ISM_UART_NUMBER == 1
    PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1;
    PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_HSI;
#elif ISM_UART_NUMBER == -1
    PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_LPUART1;
    PeriphClkInit.Lpuart1ClockSelection = RCC_LPUART1CLKSOURCE_HSI;
#else
#error unknow ISM_UART_NUMBER
#endif
    status = HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit);
    assert(status == HAL_OK);
#endif

    uart_handle.Instance        = ISM_UART;
    uart_handle.Init.BaudRate   = baudrate;
    uart_handle.Init.WordLength = word_length;
    uart_handle.Init.StopBits   = UART_STOPBITS_1;
    uart_handle.Init.Parity     = parity;
    uart_handle.Init.HwFlowCtl  = flow_control;
    uart_handle.Init.Mode       = UART_MODE_TX_RX;

#ifdef ISM_UART_SWAP_TX_RX_PINS
    uart_handle.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_SWAP_INIT;
    uart_handle.AdvancedInit.Swap = UART_ADVFEATURE_SWAP_ENABLE;
#endif

    status = HAL_UART_Init(&uart_handle);
    assert(status == HAL_OK);

#ifdef STM32L4
    // Allow wakeup from stop mode
    __HAL_UART_ENABLE_IT(&uart_handle, UART_IT_WUF);

    UART_WakeUpTypeDef wakeup;
    wakeup.WakeUpEvent = UART_WAKEUP_ON_READDATA_NONEMPTY;
    status = HAL_UARTEx_StopModeWakeUpSourceConfig(&uart_handle, wakeup);
    assert(status == HAL_OK);
    status = HAL_UARTEx_EnableStopMode(&uart_handle);
    assert(status == HAL_OK);
#endif

    // DMA rx init
#ifdef STM32L4
    dma_rx_handle.Instance = ISM_UART_DMA_RX_CHANNEL;
    dma_rx_handle.Init.Request = ISM_UART_DMA_REQUEST;
#endif
#ifdef STM32F2xx
    dma_rx_handle.Instance = ISM_UART_DMA_RX_STREAM;
    dma_rx_handle.Init.Channel = ISM_UART_DMA_CHANNEL;
#endif
    dma_rx_handle.Init.Direction = DMA_PERIPH_TO_MEMORY;
    dma_rx_handle.Init.PeriphInc = DMA_PINC_DISABLE;
    dma_rx_handle.Init.MemInc = DMA_MINC_ENABLE;
    dma_rx_handle.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    dma_rx_handle.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
    dma_rx_handle.Init.Mode = DMA_CIRCULAR;
    dma_rx_handle.Init.Priority = DMA_PRIORITY_LOW;
    status = HAL_DMA_Init(&dma_rx_handle);
    assert(status == HAL_OK);

    __HAL_LINKDMA(&uart_handle, hdmarx, dma_rx_handle);

    // DMA tx init
#ifdef STM32L4
    dma_tx_handle.Instance = ISM_UART_DMA_TX_CHANNEL;
    dma_tx_handle.Init.Request = ISM_UART_DMA_REQUEST;
#endif
#ifdef STM32F2xx
    dma_tx_handle.Instance = ISM_UART_DMA_TX_STREAM;
    dma_tx_handle.Init.Channel = ISM_UART_DMA_CHANNEL;
#endif
    dma_tx_handle.Init.Direction = DMA_MEMORY_TO_PERIPH;
    dma_tx_handle.Init.PeriphInc = DMA_PINC_DISABLE;
    dma_tx_handle.Init.MemInc = DMA_MINC_ENABLE;
    dma_tx_handle.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    dma_tx_handle.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
    dma_tx_handle.Init.Mode = DMA_NORMAL;
    dma_tx_handle.Init.Priority = DMA_PRIORITY_LOW;
    status = HAL_DMA_Init(&dma_tx_handle);
    assert(status == HAL_OK);

    __HAL_LINKDMA(&uart_handle, hdmatx, dma_tx_handle);

    // interrupt init
    HAL_NVIC_SetPriority(ISM_UART_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(ISM_UART_IRQn);

    HAL_NVIC_SetPriority(ISM_UART_DMA_RX_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(ISM_UART_DMA_RX_IRQn);

    HAL_NVIC_SetPriority(ISM_UART_DMA_TX_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(ISM_UART_DMA_TX_IRQn);
}

void buffered_uart_deinit(void)
{
    HAL_UART_DeInit(&uart_handle);
    HAL_DMA_DeInit(uart_handle.hdmarx);
    HAL_DMA_DeInit(uart_handle.hdmatx);

    HAL_NVIC_DisableIRQ(ISM_UART_IRQn);
    HAL_NVIC_DisableIRQ(ISM_UART_DMA_RX_IRQn);
    HAL_NVIC_DisableIRQ(ISM_UART_DMA_TX_IRQn);
}

void buffered_uart_start(void)
{
    HAL_UART_Receive_DMA(&uart_handle, rx_buffer, RX_BUFFER_SIZE);

    CLEAR_BIT(uart_handle.Instance->CR3, USART_CR3_EIE); // For avoiding DMA abort on frame error
}

void buffered_uart_set_baudrate(uint32_t baudrate)
{
    HAL_StatusTypeDef status;

    uart_handle.Init.BaudRate = baudrate;
    status = HAL_UART_Init(&uart_handle);
    assert(status == HAL_OK);
}

uint16_t buffered_uart_read(uint8_t* data, uint16_t size)
{
    uint16_t effective_size;

    // calculate the effective read size
    update_rx_buffer_fill_count();
    if (size < rx_buffer_fill_count) {
        effective_size = size;
    } else {
        effective_size = rx_buffer_fill_count;
    }

    if (effective_size > 0) {
        if (rx_buffer_read_index + effective_size > RX_BUFFER_SIZE) {
            uint16_t first_part_size = RX_BUFFER_SIZE - rx_buffer_read_index;
            memcpy(data, &rx_buffer[rx_buffer_read_index], first_part_size);
            memcpy(&data[first_part_size], rx_buffer, effective_size - first_part_size);
            rx_buffer_read_index = effective_size - first_part_size;
        } else {
            memcpy(data, &rx_buffer[rx_buffer_read_index], effective_size);
            rx_buffer_read_index += effective_size;
            if (rx_buffer_read_index == RX_BUFFER_SIZE) rx_buffer_read_index = 0;
        }
    }
    return effective_size;
}

uint16_t buffered_uart_write(const uint8_t* data, uint16_t size)
{
    uint16_t effective_size;

    effective_size = TX_BUFFER_SIZE - tx_buffer_fill_count;
    if (size < effective_size) effective_size = size;

    if (effective_size > 0) {
        if (tx_buffer_write_index + effective_size > TX_BUFFER_SIZE) {
            uint16_t first_part_size = TX_BUFFER_SIZE - tx_buffer_write_index;
            memcpy(&tx_buffer[tx_buffer_write_index], data, first_part_size);
            memcpy(tx_buffer, &data[first_part_size], effective_size - first_part_size);
            tx_buffer_write_index = effective_size - first_part_size;
        } else {
            memcpy(&tx_buffer[tx_buffer_write_index], data, effective_size);
            tx_buffer_write_index += effective_size;
            if (tx_buffer_write_index == TX_BUFFER_SIZE) tx_buffer_write_index = 0;
        }

        uint16_t fillCount;
        do {
            fillCount = __LDREXH(&tx_buffer_fill_count);
        } while (__STREXH(fillCount + effective_size, &tx_buffer_fill_count));
        if (fillCount == 0) start_tx_dma();
    }

    return effective_size;
}

uint16_t buffered_uart_get_read_available(void)
{
    update_rx_buffer_fill_count();
    return rx_buffer_fill_count;
}

uint16_t buffered_uart_get_write_available(void)
{
    return TX_BUFFER_SIZE - tx_buffer_fill_count;
}

#ifdef STM32L4
bool buffered_uart_is_busy(void)
{
    return (tx_buffer_fill_count > 0) ||
            (__HAL_UART_GET_FLAG(&uart_handle, USART_ISR_BUSY) == SET);
}
#endif

#ifdef STM32F2XX
bool buffered_uart_is_busy(void)
{
    return (tx_buffer_fill_count > 0) ||
            (__HAL_UART_GET_FLAG(&uart_handle, UART_FLAG_TC) == RESET);
}
#endif

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *uart_handle)
{
    uint16_t fillCount;
    do {
        fillCount = __LDREXH(&tx_buffer_fill_count);
    } while (__STREXH(fillCount-tx_dma_size, &tx_buffer_fill_count));
    //txBufferFillCount -= txDmaSize;

    tx_buffer_read_index = (tx_buffer_read_index + tx_dma_size) % TX_BUFFER_SIZE;

    if (tx_buffer_fill_count > 0) start_tx_dma();
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *uart_handle)
{
    // Rx DMA is in circular mode => nothing to do
}
uint32_t err_cnt = 0;
void HAL_UART_ErrorCallback(UART_HandleTypeDef *uart_handle) {
    //assert(0);
    err_cnt++;
    //CLEAR_BIT(uart_handle->Instance->CR3, USART_CR3_EIE); // For avoiding DMA abort on frame error
}

void ISM_UART_IRQHandler(void)
{
    HAL_UART_IRQHandler(&uart_handle);
    __HAL_UART_CLEAR_FLAG(&uart_handle, UART_CLEAR_FEF); // todo why framing error occurs ?
}

void ISM_UART_DMA_TX_IRQHandler(void)
{
    HAL_DMA_IRQHandler(&dma_tx_handle);
}

void ISM_UART_DMA_RX_IRQHandler(void)
{
    HAL_DMA_IRQHandler(&dma_rx_handle);
}

/* Private functions implementation ------------------------------------------*/

static void update_rx_buffer_fill_count(void)
{
    uint16_t write_index = RX_BUFFER_SIZE - __HAL_DMA_GET_COUNTER(uart_handle.hdmarx);
    if (write_index >= rx_buffer_read_index) {
        rx_buffer_fill_count = write_index - rx_buffer_read_index;
    } else {
        rx_buffer_fill_count = write_index + RX_BUFFER_SIZE - rx_buffer_read_index;
    }
}

static void start_tx_dma(void)
{
    tx_dma_size = tx_buffer_fill_count;
    if (tx_buffer_read_index + tx_dma_size > TX_BUFFER_SIZE) {
        tx_dma_size = TX_BUFFER_SIZE - tx_buffer_read_index;
    }

    HAL_UART_Transmit_DMA(&uart_handle, &tx_buffer[tx_buffer_read_index], tx_dma_size);
}
