/**
 ******************************************************************************
 * @file    ism3.c
 * @author  nicolas.brunner@heig-vd.ch
 * @date    06-August-2018
 * @brief   Driver for the RM1S3
 ******************************************************************************
 * @copyright HEIG-VD
 *
 * License information
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/

#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "commands_RM1S3.h"
#include "framed_uart.h"
#include "ism3.h"
#include "radio_config.h"
#include "util.h"

/* Private typedef -----------------------------------------------------------*/

typedef struct {
    GPIO_TypeDef* GPIOx;
    uint16_t GPIO_Pin;
} pin_t;

typedef struct {
    pin_t pin;
    uint32_t speed;
    uint32_t mode;
    uint32_t pull;
    uint32_t alternate;
} init_gpio_t;

typedef enum {
    RM_OFF,
    RM_TDMA,
    RM_TEST,
} radio_mode_t;

/* Private define ------------------------------------------------------------*/

#define RESET_DURATION            10
#define START_DURATION            500
#define FRAME_TIMEOUT             150
#define BAUDRATE_CHANGE_DURATION  5

#define TX_UNICAST_HEADER_SIZE   4
#define TX_MULTICAST_HEADER_SIZE 8
#define RX_HEADER_SIZE           6
#define SOURCE_INDEX             2
#define DATA_SLOT_INDEX          3
#define RSSI_INDEX               4
#define LQI_INDEX                5

#define RX_MULTICAST_HEADER_SIZE     11
#define RX_MULTICAST_SOURCE_INDEX     2
#define RX_MULTICAST_GROUP_INDEX      3
#define RX_MULTICAST_COUNTDOWN_INDEX  7
#define RX_MULTICAST_RSSI_INDEX       9
#define RX_MULTICAST_LQI_INDEX       10
#define GROUP_SIZE                    4

#define NUMBER_OF_GPIO 11
#define MAX_COMMAND_SIZE 18
#define NUMBER_OF_RECONFIGURATION_CMD 11
#define NUMBER_OF_DISCONNECT_CMD      14
#define STATE_UNINITIALIZED 0xFF

#define TX_STATUS_NONE     0
#define TX_STATUS_WAIT_ACK 1
#define TX_STATUS_ACK      2
#define TX_STATUS_TIMEOUT  3

#define DEFAULT_PHY1 0 // default phy (ETSI)
#define DEFAULT_PHY2 3 // default phy when the first one failed (FCC)
#define UNALLOWED_PHY1 1 // Phy use for the test bench
#define UNALLOWED_PHY2 2 // Phy use for the test bench

#define DEFAULT_POWER     0x06 // ~+14dBm
#define DEFAULT_POWER_DBM 14

/* Private typedef -----------------------------------------------------------*/

typedef const uint8_t commands_t[][MAX_COMMAND_SIZE];

/* Private variables ---------------------------------------------------------*/

static const init_gpio_t init_table[NUMBER_OF_GPIO] = {
    {ISM_TX,     GPIO_SPEED_FREQ_HIGH, GPIO_MODE_AF_PP,      GPIO_NOPULL, ISM_UART_AF},
    {ISM_RX,     GPIO_SPEED_FREQ_HIGH, GPIO_MODE_AF_PP,      GPIO_NOPULL, ISM_UART_AF},
#if ISM_UART_HWCONTROL == UART_HWCONTROL_NONE
    {ISM_RTS,    GPIO_SPEED_FREQ_HIGH, GPIO_MODE_OUTPUT_PP,  GPIO_NOPULL},
    {ISM_CTS,    GPIO_SPEED_FREQ_HIGH, GPIO_MODE_INPUT,      GPIO_NOPULL},
#else
    {ISM_RTS,    GPIO_SPEED_FREQ_HIGH, GPIO_MODE_AF_PP,      GPIO_NOPULL, ISM_UART_AF},
    {ISM_CTS,    GPIO_SPEED_FREQ_HIGH, GPIO_MODE_AF_PP,      GPIO_NOPULL, ISM_UART_AF},
#endif
    {ISM_GPIO0,  GPIO_SPEED_FREQ_HIGH, GPIO_MODE_INPUT,      GPIO_NOPULL},
    {ISM_GPIO1,  GPIO_SPEED_FREQ_HIGH, GPIO_MODE_IT_FALLING, GPIO_NOPULL},
    {ISM_GPIO2,  GPIO_SPEED_FREQ_HIGH, GPIO_MODE_INPUT,      GPIO_PULLDOWN},
    {ISM_GPIO3,  GPIO_SPEED_FREQ_HIGH, GPIO_MODE_INPUT,      GPIO_PULLDOWN},
    {ISM_NRESET, GPIO_SPEED_FREQ_LOW,  GPIO_MODE_OUTPUT_PP,  GPIO_NOPULL},
//    {ISM_NRESET, GPIO_SPEED_FREQ_LOW,  GPIO_MODE_INPUT,      GPIO_NOPULL}, // use for allowing JTAG connection to RM1
    {ISM_BOOT0,  GPIO_SPEED_FREQ_LOW,  GPIO_MODE_OUTPUT_PP,  GPIO_NOPULL},
    {ISM_NPEN,   GPIO_SPEED_FREQ_LOW,  GPIO_MODE_OUTPUT_PP,  GPIO_NOPULL},
};

static commands_t configuration_commands = {
    {0x04, CMD_SET_HOST_BAUDRATE,     ISM_UART_BAUDRATE / 65536, (ISM_UART_BAUDRATE / 256) % 256, ISM_UART_BAUDRATE % 256},
    {0x01, CMD_GET_FIRMWARE_VERSION},
    {0x01, CMD_GET_HARDWARE_VERSION},
    {0x01, CMD_GET_PHYS_CHANNELS_PLAN_SIZE},
#ifdef ISM_PATTERN
    {0x05, CMD_SET_PATTERN,           ISM_PATTERN},
#endif
    {0x02, CMD_SET_TX_RETRY_COUNT,    ISM_RETRY_COUNT},
    {0x02, CMD_SET_DATA_SLOT_COUNT,   ISM_DATA_SLOT_COUNT},
    {0x02, CMD_SET_GPIO0_SIGNAL,      1}, // GPIO0 = SIG_SYNC_OUT
    {0x02, CMD_SET_GPIO1_SIGNAL,      4}, // GPIO1 = SIG_TX_PENDING_OUT
    {0x0A, CMD_SET_SYNC_RX,           ISM_SYNC_RX_INTERVAL, ISM_SYNC_RX_MISS_MAX,
            ISM_SCAN_FIRST_ON_DURATION / 256, ISM_SCAN_FIRST_ON_DURATION % 256,
            ISM_SCAN_ON_DURATION / 256, ISM_SCAN_ON_DURATION % 256,
            ISM_SCAN_OFF_DURATION / 65536, (ISM_SCAN_OFF_DURATION / 256) % 256, ISM_SCAN_OFF_DURATION % 256},
    {0x02, CMD_SET_EVENT_INDICATION,  1}, // Enable IND_TDMA_STATE_CHANGED

    // Following parameters are sent after an ism_disconnect(), the numbers of commands should be set in NUMBER_OF_DISCONNECT_CMD
    {0x02, CMD_SET_RADIO_MODE,        0x00}, // Stop TDMA
    {0x02, CMD_SET_RF_PHY,            0x00}, // use phy as parameter
    {0x02, CMD_SET_ACTIVE_CHANNELS,   0x00}, // use channels as parameter

    // Following parameters can change during execution, the numbers of commands should be set in NUMBER_OF_RECONFIGURATION_CMD
    {0x02, CMD_SET_SYNC_MODE,         0x00}, // use sync_mode as parameter
    {0x06, CMD_SET_SYNC_RX_LOW_POWER, 0x00, 0x00, 0x00, 0x00, 25}, // use group as parameter, sync interval = 25
    {0x02, CMD_SET_RF_POWER,          0x00}, // use power as parameter
    {0x02, CMD_SET_ADDRESS,           0x00}, // use address as parameter
    {0x05, CMD_SET_GROUP,             0x00, 0x00, 0x00, 0x00}, // use group as parameter
    {0x09, CMD_SET_SYNC_BEACON_ID, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, // use beacon_id as parameter
    {0x09, CMD_SET_ASSOCIATED_BEACON_ID, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, // use associated_beacon_id as parameter
    {0x02, CMD_SET_ACCEPT_UNASSOCIATED, 0x00}, // use accept_unassociated as parameter
    {0x04, CMD_SET_DATA_SLOT_RANGE_TYPE, 0, 254, 0}, // deactivate all data slots
    {0x03, CMD_SET_DATA_SLOT_TYPE,    0x00, 0x00},
    {0x02, CMD_SET_RADIO_MODE,        0x01}, // Start TDMA, use radio_mode as parameter
    {0x00}
};

static ism_unicast_function_t rx_unicast_function;
static ism_multicast_function_t rx_multicast_function;
static ism_beacon_data_function_t beacon_data_function;
static ism_state_function_t state_function;
static ism_stat_function_t stat_function;
static uint8_t state = STATE_UNINITIALIZED;
static uint8_t inner_state;
static ism_state_t ism_state;
static bool wait_response = false;
static uint8_t address;
static uint32_t group = 0xFFFFFFFF;
static uint8_t power = DEFAULT_POWER;
static uint8_t power_dbm = DEFAULT_POWER_DBM;
static uint64_t associated_beacon_id;
static uint64_t beacon_id = 0;
static bool accept_unassociated = true;
static ism_sync_mode_t sync_mode = SM_RX_LOW_POWER_GROUP;
static radio_mode_t radio_mode = RM_TDMA;
static uint8_t tx_status = TX_STATUS_NONE;
static bool need_reconfiguration = false;

static char firmware_version[32];
static char hardware_version[32];
static uint8_t phys_channels_size[256];
static uint8_t phy_count;

static uint32_t frame_error_counter = 0;
static uint32_t frame_ok_counter = 0;
static uint32_t uart_rx_counter = 0;

static uint8_t phy;
static uint8_t channels[16];

/* Private functions declaration ---------------------------------------------*/

static bool is_initialized(void);
static void framed_rx(const uint8_t* data, uint16_t size);
static bool send_command(const uint8_t* data);
static void send_init_command(void);
static void set_default_phy(void);

/* Public functions ----------------------------------------------------------*/

void ism_init(
        ism_unicast_function_t rx_unicast_function_,
        ism_multicast_function_t rx_multicast_function_,
        ism_beacon_data_function_t beacon_data_function_,
        ism_state_function_t state_function_,
        ism_stat_function_t stat_function_)
{
    rx_unicast_function = rx_unicast_function_;
    rx_multicast_function = rx_multicast_function_;
    beacon_data_function = beacon_data_function_;
    state_function = state_function_;
    stat_function = stat_function_;

    state = 0;
    inner_state = 0;
    ism_state = ISM_NOT_SYNCHRONIZED;

    GPIO_InitTypeDef GPIO_InitStructure;

    for (int i = 0; i < NUMBER_OF_GPIO; i++) {
        if (init_table[i].pin.GPIOx != NULL) {
            GPIO_InitStructure.Pin = init_table[i].pin.GPIO_Pin;
            GPIO_InitStructure.Speed = init_table[i].speed;
            GPIO_InitStructure.Mode = init_table[i].mode;
            GPIO_InitStructure.Pull = init_table[i].pull;
            GPIO_InitStructure.Alternate = init_table[i].alternate;
            HAL_GPIO_Init(init_table[i].pin.GPIOx, &GPIO_InitStructure);
        }
    }

#if ISM_UART_HWCONTROL == UART_HWCONTROL_NONE
    HAL_GPIO_WritePin(((pin_t)ISM_RTS).GPIOx, ((pin_t)ISM_RTS).GPIO_Pin, GPIO_PIN_SET);
#endif

    if (((pin_t)ISM_BOOT0).GPIOx != NULL) {
        HAL_GPIO_WritePin(((pin_t)ISM_BOOT0).GPIOx, ((pin_t)ISM_BOOT0).GPIO_Pin, GPIO_PIN_RESET);
    }

    if (((pin_t)ISM_NPEN).GPIOx != NULL) {
        HAL_GPIO_WritePin(((pin_t)ISM_NPEN).GPIOx, ((pin_t)ISM_NPEN).GPIO_Pin, GPIO_PIN_RESET);
    }

    if (((pin_t)ISM_NRESET).GPIOx != NULL) {
        HAL_GPIO_WritePin(((pin_t)ISM_NRESET).GPIOx, ((pin_t)ISM_NRESET).GPIO_Pin, GPIO_PIN_RESET);
        HAL_Delay(RESET_DURATION);
        HAL_GPIO_WritePin(((pin_t)ISM_NRESET).GPIOx, ((pin_t)ISM_NRESET).GPIO_Pin, GPIO_PIN_SET);
        HAL_Delay(START_DURATION);
    }

    framed_uart_init(framed_rx, FRAME_TIMEOUT);
    framed_uart_start();

    // Use interrupt to wakeup when no more tx pending
    HAL_NVIC_SetPriority(ISM_GPIO1_EXTI_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(ISM_GPIO1_EXTI_IRQn);
}

void ism_config(uint8_t address_, uint32_t group_, uint8_t power_, uint8_t power_dbm_, uint64_t associated_beacon_id_)
{
    assert(address != 0xFF);
    address = address_;
    group = group_;
    power = power_;
    power_dbm = power_dbm_;
    associated_beacon_id = associated_beacon_id_;

    if (is_initialized()) {
        inner_state = 0;
        state -= NUMBER_OF_RECONFIGURATION_CMD;
    } else {
        need_reconfiguration = true;
    }
}

void ism_get_config(uint8_t* address_, uint32_t* group_, uint8_t* power_, uint8_t* power_dbm_, uint64_t* associated_beacon_id_)
{
    *address_ = address;
    *group_ = group;
    *power_ = power;
    *power_dbm_ = power_dbm;
    *associated_beacon_id_ = associated_beacon_id;
}

bool ism_set_phy(uint8_t phy_, const uint8_t* channels_)
{
    uint8_t size = ism_get_channels_size(phy_);
    // phy_ is valid if it's channels size is more than zero
    if (size > 0) {
        phy = phy_;

        uint16_t sum = 0;
        for (int i = 0; i < size; i++) {
            sum += channels_[i];
        }

        if (sum == 0) {
            // No channel are set => use default value with all channels in use
            memset(channels, 0xFF, size);
        } else {
            memcpy(channels, channels_, size);
        }
    }
    return size > 0;
}

void ism_disconnect(void)
{
    if (is_initialized()) {
        inner_state = 0;
        state -= NUMBER_OF_DISCONNECT_CMD; // Stop TDMA and restart it
        assert(configuration_commands[state][1] == CMD_SET_RADIO_MODE); // assert if NUMBER_OF_DISCONNECT_CMD is wrong
    }
}

void ism_set_sync_mode(ism_sync_mode_t mode)
{
    uint8_t buffer[3];

    assert(mode <= SM_RX_LOW_POWER_GROUP);

    if (sync_mode != mode) {
        if (is_initialized()) {
            buffer[0] = 2;
            buffer[1] = CMD_SET_SYNC_MODE;
            buffer[2] = mode;

            if (send_command(buffer)) {
                sync_mode = mode;
            }
        } else {
            need_reconfiguration = true;
            sync_mode = mode;
        }
    }
}

void ism_enable(bool enable)
{
    uint8_t buffer[3];

    radio_mode_t mode = enable ? RM_TDMA : RM_OFF;

    if (radio_mode != mode) {
        if (is_initialized()) {
            buffer[0] = 2;
            buffer[1] = CMD_SET_RADIO_MODE;
            buffer[2] = mode;

            if (send_command(buffer)) {
                radio_mode = mode;
            }
        } else {
            need_reconfiguration = true;
            radio_mode = mode;
        }
    }
}

bool ism_set_accept_unassociated(bool accept)
{
    uint8_t buffer[3];

    if (sync_mode != SM_TX) return false;

    if (accept_unassociated != accept) {
        if (is_initialized()) {
            buffer[0] = 2;
            buffer[1] = CMD_SET_ACCEPT_UNASSOCIATED;
            buffer[2] = accept ? 1 : 0;

            if (send_command(buffer)) {
                accept_unassociated = accept;
            } else {
                return false;
            }
        } else {
            need_reconfiguration = true;
            accept_unassociated = accept;
        }
    }

    return true;
}

bool ism_set_sync_beacon_id(uint64_t id)
{
    uint8_t buffer[10];

    if (sync_mode != SM_TX) return false;

    if (beacon_id != id) {
        if (is_initialized()) {
            buffer[0] = 9;
            buffer[1] = CMD_SET_SYNC_BEACON_ID;
            util_uint64_to_byte_array(id, &buffer[2]);

            if (send_command(buffer)) {
                beacon_id = id;
            } else {
                return false;
            }
        } else {
            need_reconfiguration = true;
            beacon_id = id;
        }
    }

    return true;
}

void ism_power_down(void)
{
    framed_uart_deinit();

    GPIO_InitTypeDef GPIO_InitStructure;

    // Put all pins in output with low level
    for (int i = 0; i < NUMBER_OF_GPIO; i++) {
        if (init_table[i].pin.GPIOx != NULL) {
            HAL_GPIO_DeInit(init_table[i].pin.GPIOx, init_table[i].pin.GPIO_Pin);

            GPIO_InitStructure.Pin = init_table[i].pin.GPIO_Pin;
            GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_LOW;
            GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
            GPIO_InitStructure.Pull = GPIO_NOPULL;
            GPIO_InitStructure.Alternate = 0;
            HAL_GPIO_Init(init_table[i].pin.GPIOx, &GPIO_InitStructure);

            HAL_GPIO_WritePin(init_table[i].pin.GPIOx, init_table[i].pin.GPIO_Pin, GPIO_PIN_RESET);
        }
    }

    if (((pin_t)ISM_NPEN).GPIOx != NULL) {
        HAL_GPIO_WritePin(((pin_t)ISM_NPEN).GPIOx, ((pin_t)ISM_NPEN).GPIO_Pin, GPIO_PIN_SET);
    }

    ism_state = ISM_OFF;
}

void ism_tick(void)
{
    assert(state != STATE_UNINITIALIZED);
    framed_uart_tick();

    if (!wait_response) {
        if (!is_initialized()) {
            send_init_command();
        }
    }
}

bool ism_is_tx_pending(void)
{
    return (tx_status == TX_STATUS_WAIT_ACK) ||
            HAL_GPIO_ReadPin(
            ((pin_t)ISM_GPIO1).GPIOx,
            ((pin_t)ISM_GPIO1).GPIO_Pin) == GPIO_PIN_SET;
}

bool ism_is_ready(void)
{
    //return ism_is_initialized() && !ism_is_tx_pending() && !framed_uart_is_busy();
    return is_initialized() && !framed_uart_is_busy();
}

bool ism_tx_unicast(uint8_t destination, const uint8_t* data, uint8_t size)
{
    uint8_t buffer[ISM_MAX_UNICAST_DATA_SIZE + TX_UNICAST_HEADER_SIZE];

    if (is_initialized() && (ism_state > ISM_NOT_SYNCHRONIZED) && !ism_is_tx_pending() && (size > 0) && (size <= ISM_MAX_UNICAST_DATA_SIZE)) {
        tx_status = TX_STATUS_WAIT_ACK;

        buffer[0] = size + TX_UNICAST_HEADER_SIZE - 1;
        buffer[1] = CMD_SEND_UNICAST;
        buffer[2] = destination;
        buffer[3] = 0xFF; // no dataslot restriction
        memcpy(&buffer[TX_UNICAST_HEADER_SIZE], data, size);
        return send_command(buffer);
    }
    return false;
}

bool ism_tx_multicast(uint32_t group, uint8_t number, const uint8_t* data, uint8_t size)
{
    uint8_t buffer[ISM_MAX_MULTICAST_DATA_SIZE + TX_MULTICAST_HEADER_SIZE];

    if (is_initialized() && (ism_state > ISM_NOT_SYNCHRONIZED) && !ism_is_tx_pending() && (size > 0) && (size <= ISM_MAX_MULTICAST_DATA_SIZE)) {
        tx_status = TX_STATUS_WAIT_ACK;

        buffer[0] = size + TX_MULTICAST_HEADER_SIZE - 1;
        buffer[1] = CMD_SEND_MULTICAST;
        util_uint32_to_byte_array(group, &buffer[2]);
        buffer[6] = number;
        buffer[7] = 0xFF; // no dataslot restriction
        memcpy(&buffer[TX_MULTICAST_HEADER_SIZE], data, size);
        return send_command(buffer);
    }
    return false;
}

bool ism_set_sync_user_data(const uint8_t* data, uint8_t size)
{
    uint8_t buffer[ISM_MAX_SYNC_USER_DATA_SIZE + 2];

    if (is_initialized() && (sync_mode == SM_TX) && (size <= ISM_MAX_SYNC_USER_DATA_SIZE)) {
        buffer[0] = size + 1;
        buffer[1] = CMD_SET_SYNC_USER_DATA;
        memcpy(&buffer[2], data, size);
        return send_command(buffer);
    }
    return false;
}

uint8_t ism_get_max_unicast_data_size(void)
{
    return ISM_MAX_UNICAST_DATA_SIZE;
}

uint8_t ism_get_max_multicast_data_size(void)
{
    return ISM_MAX_MULTICAST_DATA_SIZE;
}

char* ism_get_firmware_version(void)
{
    return firmware_version;
}

uint32_t ism_get_firmware_version_value(void)
{
    int result;
    unsigned int a, b, c;

    result = sscanf(firmware_version, "RM1S3:%u.%u.%u", &a, &b, &c);
    if (result == 3) {
        return (a << 16) + (b << 8) + c;
    } else {
        return 0;
    }
}

char* ism_get_hardware_version(void)
{
    return hardware_version;
}

bool ism_request_stat(void)
{
    uint8_t buffer[2];

    if (is_initialized()) {
        buffer[0] = 1;
        buffer[1] = CMD_GET_STAT;

        return send_command(buffer);
    }
    return false;
}

ism_state_t ism_get_state(void)
{
    return ism_state;
}

bool ism_request_state(void)
{
    uint8_t buffer[2];

    if (is_initialized()) {
        buffer[0] = 1;
        buffer[1] = CMD_GET_PROTOCOL_STATE;

        return send_command(buffer);
    }
    return false;
}

uint32_t ism_get_uart_rx_counter(void)
{
    return uart_rx_counter;
}

uint8_t ism_get_channels_size(uint8_t phy)
{
    if (phy < phy_count) {
        return phys_channels_size[phy];
    } else {
        return 0;
    }
}

void ISM_GPIO1_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(((pin_t)ISM_GPIO1).GPIO_Pin);
}

/* Private functions implementation ------------------------------------------*/

static bool is_initialized(void)
{
    assert(state != STATE_UNINITIALIZED);
    return (configuration_commands[state][0] == 0);
}

static bool send_command(const uint8_t* data)
{
    bool result = framed_uart_tx(data, data[0] + 1);
    if (result) {
        wait_response = true;
    }
    return result;
}

static void send_init_command(void)
{
    static uint8_t command[MAX_COMMAND_SIZE];

    memcpy(command, configuration_commands[state], configuration_commands[state][0] + 1);

    if (command[1] == CMD_SET_RF_PHY) {
        command[2] = phy;
    } else if (command[1] == CMD_SET_ACTIVE_CHANNELS) {
        uint8_t size = ism_get_channels_size(phy);
        command[0] = size + 1;
        memcpy(&command[2], channels, size);
    } else if (command[1] == CMD_SET_SYNC_MODE) {
        command[2] = sync_mode;
    } else if ((command[1] == CMD_SET_RADIO_MODE) && (command[2] == 1)) {
        command[2] = radio_mode;
    } else if (command[1] == CMD_SET_SYNC_RX_LOW_POWER) {
        util_uint32_to_byte_array(group, &command[2]);
    } else if (command[1] == CMD_SET_ADDRESS) {
        command[2] = address;
    } else if (command[1] == CMD_SET_GROUP) {
        util_uint32_to_byte_array(group, &command[2]);
    } else if (command[1] == CMD_SET_SYNC_BEACON_ID) {
        util_uint64_to_byte_array(beacon_id, &command[2]);
    } else if (command[1] == CMD_SET_ASSOCIATED_BEACON_ID) {
        util_uint64_to_byte_array(associated_beacon_id, &command[2]);
    } else if (command[1] == CMD_SET_ACCEPT_UNASSOCIATED) {
        command[2] = accept_unassociated;
    } else if (command[1] == CMD_SET_RF_POWER) {
        if (power != ISM_INVALID_POWER) {
            command[2] = power;
        } else {
            command[1] = CMD_SET_RF_POWER_DBM;
            command[2] = power_dbm;
        }
    } else if (command[1] == CMD_SET_DATA_SLOT_RANGE_TYPE) {
        if (address == 0) {
            // server config => all dataslot in rx except the first that will be set with CMD_SET_DATA_SLOT_TYPE
            command[4] = 0x01; // rx
        }
    } else if (command[1] == CMD_SET_DATA_SLOT_TYPE) {
        if (address == 0) {
            // server config => first slot is for tx
            command[2] = 0;
            command[3] = 0x02; // tx
            inner_state = 1;
        } else {
            // client config
            if (inner_state == 0) {
                // server slot
                command[2] = 0;
                command[3] = 0x01; // rx
            } else if (inner_state == 1) {
                // own slot
                command[2] = address;
                command[3] = 0x02; // tx
            }
        }
    }

    send_command(command);
}

static void framed_rx(const uint8_t* data, uint16_t size)
{

    // timeout
    if (data[0] != (size - 1)) {
        return;
    } else {
        uart_rx_counter++;
    }

    if (!is_initialized()) {
        if ((data[0] == 1) && (data[1] == CMD_SET_HOST_BAUDRATE)) {
            framed_uart_set_baudrate(ISM_UART_BAUDRATE);
            state++;
            wait_response = false;
            HAL_Delay(BAUDRATE_CHANGE_DURATION);
            framed_uart_flush();
        } else if ((data[0] == 1) && (data[1] == CMD_SET_DATA_SLOT_TYPE)) {
            inner_state++;
            if (inner_state == 2) {
                state++;
            }
            wait_response = false;
        } else if ((data[0] == 1) && (data[1] == configuration_commands[state][1])) {
            state++;
            wait_response = false;
        } else if ((data[0] == 1) && (data[1] == CMD_SET_RF_POWER_DBM) && (configuration_commands[state][1] == CMD_SET_RF_POWER)) {
            state++;
            wait_response = false;
        } else if (data[1] == CMD_GET_FIRMWARE_VERSION) {
            state++;
            wait_response = false;
            uint8_t length = data[0] - 1;
            if (length > sizeof(firmware_version) - 1) {
                // limit length to avoid buffer overflow
                length = sizeof(firmware_version) - 1;
            }
            memcpy(firmware_version, &data[2], length);
            firmware_version[length] = 0;
        } else if (data[1] == CMD_GET_HARDWARE_VERSION) {
            state++;
            wait_response = false;
            uint8_t length = data[0] - 1;
            if (length > sizeof(hardware_version) - 1) {
                // limit length to avoid buffer overflow
                length = sizeof(hardware_version) - 1;
            }
            memcpy(hardware_version, &data[2], length);
            hardware_version[length] = 0;
            if (state_function != NULL) state_function(ISM_VERSION_READY, 0);
        } else if (data[1] == CMD_GET_PHYS_CHANNELS_PLAN_SIZE) {
            state++;
            wait_response = false;
            phy_count = data[0] - 1;
            memcpy(phys_channels_size, &data[2], phy_count);
            phys_channels_size[UNALLOWED_PHY1] = 0;
            phys_channels_size[UNALLOWED_PHY2] = 0;
            set_default_phy();
        } else if (data[1] == IND_ERROR) {
            wait_response = false;
        }

        // When initialization is done, check if there is a need to reconfigure
        if (is_initialized() && need_reconfiguration) {
            need_reconfiguration = false;
            inner_state = 0;
            state -= NUMBER_OF_RECONFIGURATION_CMD;
        }
    }

    // Response from CMD_SEND_DATA
    if ((data[0] == 2) && (data[1] == IND_ERROR)) {
        frame_error_counter++;
        tx_status = TX_STATUS_TIMEOUT + data[2];
        wait_response = false;
    } else if ((data[0] == 1) && ((data[1] == CMD_SEND_UNICAST) || (data[1] == CMD_SEND_MULTICAST))) {
        // Frame transmitted to modem correctly
        frame_ok_counter++;
        tx_status = TX_STATUS_ACK;
        wait_response = false;
    }

    // Response from commands sent outside of initialization
    if ((data[0] == 1) && (data[1] == CMD_SET_SYNC_MODE)) {
        wait_response = false;
    } else if ((data[0] == 1) && (data[1] == CMD_SET_SYNC_USER_DATA)) {
        wait_response = false;
    } else if ((data[0] == 1) && (data[1] == CMD_SET_SYNC_BEACON_ID)) {
        wait_response = false;
    } else if ((data[0] == 1) && (data[1] == CMD_SET_ACCEPT_UNASSOCIATED)) {
        wait_response = false;
    } else if ((data[0] == 1) && (data[1] == CMD_SET_RADIO_MODE)) {
        wait_response = false;
    }

    // Response from CMD_GET_STAT
    if (((data[0] == sizeof(ism_stat_t) + 1) || (data[0] == sizeof(ism_stat_t) - 3)) && (data[1] == CMD_GET_STAT)) {
        if (stat_function != NULL) {
#ifdef ISM_RAW_STAT
            stat_function(&data[2], size - 2);
#else
            ism_stat_t stat;

            stat.rxOk = util_byte_array_to_uint32(&data[2]);
            stat.rxCrcError = util_byte_array_to_uint32(&data[6]);
            stat.tx = util_byte_array_to_uint32(&data[10]);
            stat.txLbtFail = util_byte_array_to_uint32(&data[14]);
            stat.txAck = util_byte_array_to_uint32(&data[18]);
            stat.txNack = util_byte_array_to_uint32(&data[22]);

            stat.syncRxOk = util_byte_array_to_uint32(&data[26]);
            stat.syncRxCrcError = util_byte_array_to_uint32(&data[30]);
            stat.syncRxBadFrame = util_byte_array_to_uint32(&data[34]);
            stat.syncRxTimeout = util_byte_array_to_uint32(&data[38]);
            stat.syncRxLost = util_byte_array_to_uint32(&data[42]);
            stat.syncMinDelta = util_byte_array_to_uint16(&data[46]);
            stat.syncMaxDelta = util_byte_array_to_uint16(&data[48]);
            stat.syncSumDelta = util_byte_array_to_uint32(&data[50]);

            stat.lpsyncRxOk = util_byte_array_to_uint32(&data[54]);
            stat.lpsyncRxCrcError = util_byte_array_to_uint32(&data[58]);
            stat.lpsyncRxBadFrame = util_byte_array_to_uint32(&data[62]);
            stat.lpsyncRxTimeout = util_byte_array_to_uint32(&data[66]);
            stat.lpsyncRxLost = util_byte_array_to_uint32(&data[70]);
            stat.lpsyncMinDelta = util_byte_array_to_uint16(&data[74]);
            stat.lpsyncMaxDelta = util_byte_array_to_uint16(&data[76]);
            stat.lpsyncSumDelta = util_byte_array_to_uint32(&data[78]);

            stat.syncTxOk = util_byte_array_to_uint32(&data[82]);
            stat.syncTxLbtFail = util_byte_array_to_uint32(&data[86]);

            stat.rxScanTime = util_byte_array_to_uint32(&data[90]);
            stat.rxTime = util_byte_array_to_uint32(&data[94]);
            stat.txTime = util_byte_array_to_uint32(&data[98]);

            stat.txUnicast = util_byte_array_to_uint32(&data[102]);
            stat.txUnicastAck = util_byte_array_to_uint32(&data[106]);
            stat.txUnicastNack = util_byte_array_to_uint32(&data[110]);
            stat.txMulticast = util_byte_array_to_uint32(&data[114]);
            stat.txMulticastAttempt = util_byte_array_to_uint32(&data[118]);

            stat.rxUnicastOk = util_byte_array_to_uint32(&data[122]);
            stat.rxMulticastOk = util_byte_array_to_uint32(&data[126]);
            stat.rxBadFrame = util_byte_array_to_uint32(&data[130]);
            stat.rxWrongBeaconId = util_byte_array_to_uint32(&data[134]);
            stat.rxUnicastDuplicate = util_byte_array_to_uint32(&data[138]);
            stat.rxUnicastWrongAddress = util_byte_array_to_uint32(&data[142]);
            stat.rxMulticastWrongGroup = util_byte_array_to_uint32(&data[146]);

            stat.scanOnPhase = util_byte_array_to_uint32(&data[150]);
            stat.scanLock = util_byte_array_to_uint32(&data[154]);
            stat.scanLockTimeout = util_byte_array_to_uint32(&data[158]);
            stat.scanLockRssiTooLow = util_byte_array_to_uint32(&data[162]);
            stat.scanRxCrcError = util_byte_array_to_uint32(&data[166]);
            stat.scanRxBadFrame = util_byte_array_to_uint32(&data[170]);
            stat.scanRefuseUnassociated = util_byte_array_to_uint32(&data[174]);
            if (data[0] == sizeof(ism_stat_t) + 1) {
                stat.scanRefuseBlacklisted = util_byte_array_to_uint32(&data[178]);
                stat.scanSuccess = util_byte_array_to_uint32(&data[182]);
            } else {
                stat.scanRefuseBlacklisted = 0;
                stat.scanSuccess = util_byte_array_to_uint32(&data[178]);
            }

            stat_function(stat);
#endif
        }
        wait_response = false;
    }

    // Response from CMD_GET_PROTOCOL_STATE
    if ((data[0] == 2) && (data[1] == CMD_GET_PROTOCOL_STATE)) {
        wait_response = false;
    }

    // Asynchronous frames
    if ((data[1] == IND_RECEIVED_UNICAST) && (size > RX_HEADER_SIZE)) {
        if (rx_unicast_function != NULL) rx_unicast_function(
                &data[RX_HEADER_SIZE], // data
                size - RX_HEADER_SIZE, // size
                data[SOURCE_INDEX],
                data[RSSI_INDEX],
                data[LQI_INDEX]);
    } else if ((data[1] == IND_RECEIVED_MULTICAST) && (size > RX_MULTICAST_HEADER_SIZE)) {
        if (rx_multicast_function != NULL) rx_multicast_function(
                &data[RX_MULTICAST_HEADER_SIZE], // data
                size - RX_MULTICAST_HEADER_SIZE, // size
                data[RX_MULTICAST_SOURCE_INDEX],
                data[RX_MULTICAST_COUNTDOWN_INDEX],
                data[RX_MULTICAST_RSSI_INDEX],
                data[RX_MULTICAST_LQI_INDEX]);
    } else if ((data[1] == IND_UPDATED_SYNC_DATA) && (size >= 2)) {
        if (beacon_data_function != NULL) beacon_data_function(&data[2], size - 2);
    } else if (data[1] == IND_TDMA_STATE_CHANGED) {
        if (size == 3) {
            ism_state = data[2];
            if (state_function != NULL) state_function(ism_state, 0);
        } else if (size == 11) {
            ism_state = data[2];
            if (state_function != NULL) state_function(ism_state, util_byte_array_to_uint64(&data[3]));
        }
    }
}

static void set_default_phy(void)
{
    if (ism_get_channels_size(DEFAULT_PHY1) > 0) {
        phy = DEFAULT_PHY1;
        memset(channels, 0xFF, ism_get_channels_size(DEFAULT_PHY1));
    } else {
        phy = DEFAULT_PHY2;
        memset(channels, 0xFF, ism_get_channels_size(DEFAULT_PHY2));
    }
}
