/**
 ******************************************************************************
 * @file    ism3.h
 * @author  nicolas.brunner@heig-vd.ch (Most comments from Marc Leemann)
 * @date    06-August-2018
 * @brief   Driver for the RM1S3
 ******************************************************************************
 * @copyright HEIG-VD
 *
 * License information
 *
 ******************************************************************************
 */

/**
 * @page ISM3_manual Using the ISM3 module
 *
 * # Overview
 *
 * The ISM3 radio module is a serial-accessible radio module for LPWAN
 * applications. It operates in the ISM 868 or 915 MHz band.
 *
 * Note: ISM stands for
 * <a href="https://en.wikipedia.org/wiki/ISM_radio_band">
 * Industrial-Scientific-Medical</a>,
 * which refers to the common name given to its operating band.
 * The ISM 868 band is an unlicensed (meaning anyone can use it as long as they
 * follow some common rules) band with center frequency at 868 MHz.
 * It is used for IoT applications in Europe. Its US counterpart is at 915 MHz.
 * There are other ISM bands, such as the 2.4 and 5 GHz bands used by Wi-Fi.
 *
 * # Usage
 *
 * Initialize handler functions (@ref ism_init_std_handlers), configure module
 * and set @ref ISM3_syncmodes.
 *
 * @ref ism_tick regularly to update serial RX/TX buffers.
 * Without ticking, data may neither be transmitted nor received.
 *
 * Transmit frames with @ref ism_tx_unicast.
 *
 * Receive frames with configured handlers (see @ref ism_init or
 * @ref ism_init_std_handlers).
 *
 * # Configuration
 * See @subpage ISM3_config
 *
 * # Power modes
 * See @subpage ISM3_syncmodes
 *
 * # States
 * See @subpage ISM3_states
 *
 * # Troubleshooting
 *
 * See @subpage ISM3_troubleshooting
 */
/**
 * @page ISM3_config Configuring the ISM3 module
 *
 * # Where is the config?
 *
 * Configuration settings are scattered across:
 * - @ref ism_config
 * - @ref ism_set_sync_mode
 * - the @ref configuration_commands array
 * - the #ISM_BAUDRATE macro
 *
 * # How do I change it?
 *
 * Use @ref ism_config to set address, group, power and beacon ID to sync to.
 *
 * Use @ref ism_set_sync_mode to set the desired sync mode.
 * See @ref ISM3_syncmodes for help on which one does what.
 *
 * Modify the configuration_commands array's contents to match your liking.
 * Refer to document
 * <a href="RM1S3 Host Commands.pdf">RM1S3 Host Commands</a> for settings information.
 * You can simply look up the command code and the command reference should
 * tell you what the parameter sets.
 *
 * # Default client configuration
 *
 * A default configuration is available in ism3.c.
 *
 * ## ism_config
 *
 * Call with following parameters:
 *
 * Name                 | Value
 * ---------------------|-----------
 * address_             | any > 0
 * group_               | any (=/= to allow TX/RX asynchronous frames)
 * power_               | 0x10
 * power_dbm_           | 0x12
 * associated_beacon_id | any (default: 0xD322FE7D02D3D117)
 *
 * ## ism_set_sync_mode
 *
 * Set as @ref SM_RX_ACTIVE, @ref SM_RX_LOW_POWER or @ref SM_RX_LOW_POWER_GROUP.
 * See @ref ISM3_syncmodes.
 *
 * ## configuration_commands array
 *
 * This array defines the commands that will be sent on initialization.
 * Commands will be sent in order.
 * The #NUMBER_OF_RECONFIGURATION_CMD macro defines the number of
 * commands that will be sent in case of reconfiguration.
 * Reconfiguration commands are the last ones in the array.
 *
 * @code{c}
 * static commands_t configuration_commands = {
    {0x04, CMD_SET_HOST_BAUDRATE,     ISM_UART_BAUDRATE / 65536, (ISM_UART_BAUDRATE / 256) % 256, ISM_UART_BAUDRATE % 256},
    {0x01, CMD_GET_FIRMWARE_VERSION},
    {0x01, CMD_GET_HARDWARE_VERSION},
    {0x01, CMD_GET_UNIQUE_DEVICE_ID},
    {0x05, CMD_SET_PATTERN,                 0x19, 0x17, 0x10, 0x25}, // pattern
    {0x01, CMD_GET_PHYS_CHANNELS_PLAN_SIZE},
    {0x09, CMD_SET_SYNC_BEACON_ID,          0xD3, 0x22, 0xFE, 0x7D, 0x02, 0xD3, 0xD1, 0x17}, // Gateway ID
    {0x02, CMD_SET_RF_PHY,                  0x00}, // phy ETSI
    {0x02, CMD_SET_TX_RETRY_COUNT,          0x02}, // Tx retry count = 2
    {0x02, CMD_SET_TX_RETRY_RESTRICTION,    0x00}, // Tx retry restriction = next cycle
    {0x02, CMD_SET_GPIO0_SIGNAL,            0x01}, // GPIO0 = SIG_SYNC_OUT
    {0x02, CMD_SET_GPIO1_SIGNAL,            0x02}, // GPIO1 = SIG_TIMESLOT_OUT
    {0x0A, CMD_SET_SYNC_RX, 0x01, 0x64, 0x01, 0x92, 0x00, 0xCA, 0x00, 0x03, 0xE8},
    // interval = 1, missMax = 100, initialTrack = 402ms, trackOn = 202ms, trackOff = 1s
    {0x02, CMD_SET_EVENT_INDICATION,        1},    // Enable IND_TDMA_STATE_CHANGED
    {0x02, CMD_SET_RADIO_MODE,              0x00}, // radio mode inactive

    {0x02, CMD_SET_RF_PHY,                  0x00}, // use phy as parameter
    {0x02, CMD_SET_ACTIVE_CHANNELS,         0x00}, // use channels as parameter
    //{0x02, CMD_SET_DATA_SLOT_COUNT, 0xFF},

    // Following parameters can change during execution, the numbers of commands should be set in NUMBER_OF_RECONFIGURATION_CMD
    {0x02, CMD_SET_SYNC_MODE,               0x00}, // use sync_mode as parameter
    {0x06, CMD_SET_SYNC_RX_LOW_POWER,       0x00, 0x00, 0x00, 0x00, 1}, // use group as parameter, sync interval = 1 (all beacons)
    {0x02, CMD_SET_RF_POWER,                0x00}, // use power as parameter
    {0x02, CMD_SET_ADDRESS,                 0x00}, // use address as parameter
    {0x05, CMD_SET_GROUP,                   0x00, 0x00, 0x00, 0x00}, // use group as parameter
    {0x09, CMD_SET_ASSOCIATED_BEACON_ID,    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, // use associated_beacon_id as parameter
    {0x04, CMD_SET_DATA_SLOT_RANGE_TYPE,    0, 254, 0},  // deactivate all data slots
    {0x03, CMD_SET_DATA_SLOT_TYPE,          0x00, 0x00}, // dataslot set automatically by software
    {0x02, CMD_SET_RADIO_MODE,              0x01}, // Start TDMA
    {0x00}
};
 * @endcode
 *
 * Example: if #NUMBER_OF_RECONFIGURATION_CMD=2, CMD_SET_DATA_SLOT_TYPE and
 * CMD_SET_RADIO_MODE will be sent on reconfiguration.
 *
 * ## ISM_BAUDRATE macro
 *
 * This macro defines the baudrate to use to communicate with the radio module.
 * Default value is 9600.
 * It can be set to any value under 1M but need to be lower than 30000 to achieve the lowest power mode.
 *
 * # What can I change?
 *
 * You can change host baudrate, pattern, TX retry count, GPIO signals, Sync RX,
 * RF Phy and channels, sync mode, power, address and group.
 * You should match pattern, Sync RX, RF Phy and channels to gateway configuration.
 */
/**
 * @page ISM3_syncmodes ISM3 sync modes
 *
 * The ISM module used in has several sync modes for the user to choose from.
 * The following descriptions should help the user choose the relevant mode for
 * their application.
 *
 * # ISM_TX
 *
 * Module is in gateway mode. It will be the master of its WPAN
 * (Wireless Personal Area Network).
 *
 * # SM_RX_ACTIVE
 *
 * Module is in RX mode. It will remain in SYNCHRONIZED state all the time, and
 * receive unicast and multicast frames. The module will not enter power saving
 * mode.
 *
 * # SM_RX_LOW_POWER
 *
 * Module is in low power mode. It will not react to group wakeups and remain
 * asleep. The only way for the module to receive data is to get it from a
 * beacon data change.
 *
 * # SM_RX_LOW_POWER_GROUP
 *
 * Module is in low power mode and will react to group wakeups.
 * When the gateway wakes one of the groups the module belongs to, it will enter
 * state ISM_SYNCHRONIZED.
 * When the node is synchronized, it will receive unicast and multicast frames.
 */
/**
 * @page ISM3_states ISM3 states
 *
 * # General information
 *
 * It takes approximately 15 seconds for a node to lose
 * sync with the current configuration in ism3.c.
 * This is because the node tries several times to reach the gateway until it
 * determines it has lost sync.
 *
 * # ISM_OFF
 *
 * Module is uninitialized.
 *
 * # ISM_NOT_SYNCHRONIZED
 *
 * Module is waiting for sync. It is not connected to a gateway.
 *
 * # ISM_SYNCHRONIZED
 *
 * Module is synchronized to a gateway.
 * Gets in this state on wakeup from gateway if it is in SM_RX_LOW_POWER_GROUP
 * power mode.
 * Stays in this state for as long as it is connected in SM_RX_ACTIVE power mode.
 *
 * # ISM_LOW_POWER_SYNC
 *
 * Module is synced to a gateway in SM_LOW_POWER or SM_LOW_POWER_GROUP power
 * mode, but not woken up. Cannot be woken up in SM_LOW_POWER.
 *
 * # ISM_TX_SYNC
 *
 * Module is the gateway of its WPAN.
 *
 * # ISM_VERSION_READY
 *
 * Module is getting initialized and has not received a power mode. But it will
 * respond correctly to ism_get_firmware_version() and ism_get_hardware_version().
 */
/**
 * @page ISM3_troubleshooting ISM3 Troubleshooting
 *
 * Things don't always happen as planned and that is a normal part of
 * development.
 * There are a few standard steps one can take to identify problems.
 *
 * # Documents
 *
 * <a href="RM1S3 Host Commands.pdf">ISM3 documentation</a>
 *
 * <a href="RFShield_docs.PDF">Shield schematics and layout</a>
 *
 * # Jumper setup
 *
 * There are a few jumpers to choose different configurations on the radio
 * shield:
 * - X11: power source. Left: power from NUCLEO connector.
 * Right: power from USB-C connector.
 * - X17,X18,X22,X23: UART source. Left: USB-C UART.
 * Right: UART from NUCLEO connector.
 * - X1,X5: Power supply toggles. Left jumper closes power supply circuit.
 * Right jumper closes power supply indicator LED circuit.
 * - X24: Integrated STM32 chip BOOT0 pin. Boot partition selection.
 * Do not bridge unless you know what you are doing.
 * Contact nicolas.brunner@heig-vd.ch for information.
 *
 * Reference configuration for use with STM32 dev kit is:
 *
 * Name | Side
 * -----|-----
 * X1   | On (vertical)
 * X5   | On (vertical)
 * X11  | Left
 * X17  | Right
 * X18  | Right
 * X22  | Right
 * X23  | Right
 * X24  | No jumper
 *
 * This configuration is shown on assembly picture (@ref hardware_setup).
 *
 * # Module power supply
 *
 * On the radio shield, check LEDs VCC1 and VCC_PA are lit up when jumpers X1, X5
 * are set.
 * If not lit up, check jumper X11.
 * Default setting is left for power coming from STM32 board.
 *
 * Check that E5V on STM32 board is supplied.
 * For supply through ST-Link debugger (USB connector), JP5[1-2] **and** JP5[5-6]
 * must be bridged, otherwise shield power supply is left floating.
 * See layout below.
 * @image html e5v.png "Power supply jumpers per UM2206" height=400
 * @image latex e5v.png "Power supply jumpers per UM2206" width=0.6\textwidth
 *  Refer to STM UM2206 for any additional details.
 *
 * # UART troubleshooting
 *
 * ## Check hardware connections
 * Check jumpers X17, X18, X22, X23 are correctly set.
 *
 * UART Jumper definitions:
 *
 * Name | Signal | Description
 * -----|--------|----------
 * X17  | CTS    | Clear to send
 * X18  | TX     | Transmit
 * X22  | RTS    | Request to send
 * X23  | RX     | Receive
 *
 * <a href="https://www.silabs.com/documents/public/application-notes/an0059.0-uart-flow-control.pdf">AN0059.0 from Silicon Labs</a>
 * provides a good overview of these signals and their waveforms.
 *
 * ## Check UART signals
 *
 * Using an oscilloscope or logic analyzer will
 * allow you to check for UART frames manually.
 * If shield sends an answer, you are probably on the right track!
 * See error code definitions in <a href="RM1S3 Host Commands.pdf">ISM3 doc</a>
 * and try to solve the problem.
 *
 * If shield does not send an answer, baudrate configuration may be wrong.
 * Check config using @ref ISM3_config as reference.
 *
 * ## Check STM32 serial config
 *
 * If either RX or TX does not work, check that UART and DMA RX/TX handles
 * in Core/Src/stm32l4xx_it.c match the handles in rm1s3/Src/buffered_uart.c.
 *
 * Edit DMA and UART interrupt handlers to provide the handles from buffered_uart.c.
 * Find below a sample code:
 *
 * @code{c}
 * // External variables --------------------------------------------------------
 * extern DMA_HandleTypeDef hdma_usart1_rx;
 * extern DMA_HandleTypeDef hdma_usart1_tx;
 * extern UART_HandleTypeDef huart1;
 * // USER CODE BEGIN EV
 * // modified handles in handlers, replaced by these
 * extern UART_HandleTypeDef uart_handle;
 * extern DMA_HandleTypeDef dma_rx_handle;
 * extern DMA_HandleTypeDef dma_tx_handle;
 * // USER CODE END EV
 *
 * [...]
 *
 * void DMA1_Channel4_IRQHandler(void)
 * {
 *   // USER CODE BEGIN DMA1_Channel4_IRQn 0
 *
 *   // USER CODE END DMA1_Channel4_IRQn 0
 *   HAL_DMA_IRQHandler(&dma_tx_handle);
 *   // USER CODE BEGIN DMA1_Channel4_IRQn 1
 *
 *   // USER CODE END DMA1_Channel4_IRQn 1
 * }

 * void DMA1_Channel5_IRQHandler(void)
 * {
 *   // USER CODE BEGIN DMA1_Channel5_IRQn 0
 *
 *   // USER CODE END DMA1_Channel5_IRQn 0
 *   HAL_DMA_IRQHandler(&dma_rx_handle);
 *   // USER CODE BEGIN DMA1_Channel5_IRQn 1
 *
 *   // USER CODE END DMA1_Channel5_IRQn 1
 * }
 *
 * void USART1_IRQHandler(void)
 * {
 *   // USER CODE BEGIN USART1_IRQn 0
 *
 *   // USER CODE END USART1_IRQn 0
 *   HAL_DMA_IRQHandler(&uart_handle);
 *   // USER CODE BEGIN USART1_IRQn 1
 *
 *   // USER CODE END USART1_IRQn 1
 * }
 *
 * @endcode
 *
 * This is especially likely to be the problem if you regenerated code after
 * a config change or are porting the driver to another STM32 platform with
 * different hardware.
 *
 * @todo Try different handle setups to make the library more portable.
 * Try not letting CubeIDE auto-generate a config and let ISM3 driver stack
 * configure hardware, only modifying the handle definitions in buffered_uart.c
 *
 * # Shield configuration
 *
 * With an active gateway in range, check that LEDs IO1 and IO2 light up
 * (default configuration).
 * If they don't, try to restart the program after resetting ISM3 module by
 * pressing the reset button on the left side.
 *
 * Default configuration LED signals:
 *
 * LED | Signal                 | Description
 * ----|------------------------|----
 * 1   | SIG_SYNC_OUT           | Beacon received
 * 2   | SIG_TIMESLOT_OUT       | TX/RX timeslot
 *
 * # Radio communication
 *
 * If configuration LEDs light up, radio link may be defective.
 * Check R15-R16 position matches desired antenna setup.
 * Check impedance matching on C21, C23, C24.
 * If using on-shield antenna, check C25 and C26.
 *
 */

#ifndef ISM_H
#define ISM_H

/* Includes ------------------------------------------------------------------*/

#include <stdbool.h>
#include <stdint.h>

#include "radio_config.h"

/* Exported types ------------------------------------------------------------*/

/**
 * @brief ISM TDMA state. See @ref ISM3_states
 */
typedef enum {
    ISM_OFF,
    ISM_NOT_SYNCHRONIZED,
    ISM_SYNCHRONIZED,
    ISM_LOW_POWER_SYNC,
    ISM_TX_SYNC,
    ISM_VERSION_READY,
} ism_state_t;

/**
 * @brief ISM sync mode. See @ref ISM3_syncmodes
 */
typedef enum {
    SM_TX, ///< TX mode
    SM_RX_ACTIVE, ///< RX async mode (awake)
    SM_RX_LOW_POWER, ///< RX low power mode (only beacon data)
    SM_RX_LOW_POWER_GROUP, ///< RX low power group wake mode
} ism_sync_mode_t;

/**
 * @brief Easy access to ISM statistics. Request with @ref ism_request_stat
 */
typedef struct {
    uint32_t rxOk; ///< number of data frame correctly received
    uint32_t rxCrcError; ///< number of data frame received with CRC error

    uint32_t tx; ///< number of data frame transmission
    uint32_t txLbtFail; ///< number of data frame LBT failure
    uint32_t txAck; ///< number of data frame acknowledged
    uint32_t txNack; ///< number of data frame not acknowledged

    // in synchronized mode
    uint32_t syncRxOk; ///< number of successful sync frame reception
    uint32_t syncRxCrcError; ///< number of CRC error in sync frame reception
    uint32_t syncRxBadFrame; ///< number of bad sync frame received
    uint32_t syncRxTimeout; ///< number of sync frame reception timeout
    uint32_t syncRxLost; ///< number of synchronization lost
    int16_t syncMinDelta; ///< Minimum value for the delta of synchronization in tick
    int16_t syncMaxDelta; ///< Maximum value for the delta of synchronization in tick
    int32_t syncSumDelta; ///< Sum of all the delta of synchronization in tick

    // in low power synchronized mode
    uint32_t lpsyncRxOk; ///< number of successful sync frame reception
    uint32_t lpsyncRxCrcError; ///< number of CRC error in sync frame reception
    uint32_t lpsyncRxBadFrame; ///< number of bad sync frame received
    uint32_t lpsyncRxTimeout; ///< number of sync frame reception timeout
    uint32_t lpsyncRxLost; ///< number of synchronization lost
    int16_t lpsyncMinDelta; ///< Minimum value for the delta of synchronization in tick
    int16_t lpsyncMaxDelta; ///< Maximum value for the delta of synchronization in tick
    int32_t lpsyncSumDelta; ///< Sum of all the delta of synchronization in tick

    uint32_t syncTxOk; ///< number of sync frame transmission
    uint32_t syncTxLbtFail; ///< number of sync frame LBT failure

    uint32_t rxScanTime; ///< Time spend in reception during scan in second
    uint32_t rxTime; ///< total time spend in reception (excepted scan) in second
    uint32_t txTime; ///< total time spend in transmission in second

    // transmission at user level (nack only after all retry fail)
    uint32_t txUnicast; ///< Number of unicast demand (return no error)
    uint32_t txUnicastAck; ///< Number of acknowledged unicast
    uint32_t txUnicastNack; ///< Number of not acknowledged unicast
    uint32_t txMulticast; ///< Number of multicast demand (return no error)
    uint32_t txMulticastAttempt; ///< Number of multicast transmission attempt (txMulticast * (countdown + 1))

    uint32_t rxUnicastOk; ///< Number of correctly received unicast frame
    uint32_t rxMulticastOk; ///< Number of correctly received multicast frame
    uint32_t rxBadFrame; ///< Number of wrong frame type or size in a data slot
    uint32_t rxWrongBeaconId; ///< Number of received frame with the wrong beacon id
    uint32_t rxUnicastDuplicate; ///< Number of duplicate unicast frame received
    uint32_t rxUnicastWrongAddress; ///< Number of unicast frame with wrong destination address received
    uint32_t rxMulticastWrongGroup; ///< Number of multicast frame with wrong destination group received

    uint32_t scanOnPhase; ///< Number of phase with active scanning
    uint32_t scanLock; ///< Number of time a channel is locked because of RSSI over threshold
    uint32_t scanLockTimeout; ///< Number of time a locked channel timeout because of lack of pattern received
    uint32_t scanLockRssiTooLow; ///< Number of time a locked channel end because of a RSSI under threshold
    uint32_t scanRxCrcError; ///< Number of CRC error in scan frame reception
    uint32_t scanRxBadFrame; ///< Number of bad scan frame received
    uint32_t scanRefuseUnassociated; ///< Number of synchronization fail because of beacon refuse unassociated
    uint32_t scanRefuseBlacklisted; ///< Number of sync frame received during scan that have a blacklisted beacon ID
    uint32_t scanSuccess; ///< Number of scan success
} ism_stat_t;

typedef void (*ism_unicast_function_t)(const uint8_t* data, uint8_t size, uint8_t source, int8_t rssi, uint8_t lqi);
///< type of unicast RX handler
typedef void (*ism_multicast_function_t)(const uint8_t* data, uint8_t size, uint8_t source, uint8_t countdown, int8_t rssi, uint8_t lqi);
///< type of multicast RX handler
typedef void (*ism_beacon_data_function_t)(const uint8_t* data, uint8_t size);
///< type of beacon data change handler
typedef void (*ism_state_function_t)(ism_state_t state, uint64_t gateway_id);
///< type of TDMA state change handler
#ifdef ISM_RAW_STAT
typedef void (*ism_stat_function_t)(const uint8_t* stat, uint8_t size);
#else
typedef void (*ism_stat_function_t)(ism_stat_t stat);
#endif
///< type of statistics RX handler

/* Exported preprocessor constants -------------------------------------------*/

#define ISM_TIMESLOT_DURATION       20   ///< Timeslot duration in millisecond
#define ISM_MAX_UNICAST_DATA_SIZE   239  ///< Max data size for unicast frame
#define ISM_MAX_MULTICAST_DATA_SIZE 240  ///< Max data size for multicast frame
#define ISM_MAX_SYNC_USER_DATA_SIZE 8    ///< Max user data size for sync frame
#define ISM_INVALID_POWER           0xFF ///< Power code for using the power in dBm
#define ISM_MAX_POWER               52   ///< Max power
#define ISM_MAX_POWER_DBM           30   ///< Max power in dBm

/* Exported functions --------------------------------------------------------*/

/**
 * @brief Initialize ISM module
 * @param rx_unicast_function the function called when unicast are received
 * @param rx_multicast_function the function called when multicast data are received
 * @param beacon_data_function the function called when beacon data are received
 * @param state_function the function called when the state change
 * @param stat_function the function called when stat are read
 *
 * Initialize serial communication hardware and handler functions
 */
void ism_init(
        ism_unicast_function_t rx_unicast_function,
        ism_multicast_function_t rx_multicast_function,
        ism_beacon_data_function_t beacon_data_function,
        ism_state_function_t state_function,
        ism_stat_function_t stat_function);

/**
 * @brief Configure the ISM
 * @param address module address
 * @param group module group
 * @param power desired power setting
 * @param power_dbm power in dBm, this value is only used when power == ISM_INVALID_POWER
 * @param associated_beacon_id beacon ID to synchronize to
 */
void ism_config(uint8_t address, uint32_t group, uint8_t power, uint8_t power_dbm, uint64_t associated_beacon_id);

/**
 * @brief Get current ISM config
 * @param address fill this memory zone with module address
 * @param group fill this memory zone with module group
 * @param power fill this memory zone with desired power setting
 * @param power_dbm fill this memory zone with matching power in dBm
 * @param associated_beacon_id fill this memory zone with beacon ID to synchronize to
 */
void ism_get_config(uint8_t* address, uint32_t* group, uint8_t* power, uint8_t* power_dbm, uint64_t* associated_beacon_id);

/**
 * Set the physical layer parameters, they will be use only after a ism_disconnect()
 * @param phy the phy to use
 * @param channels array where each bit indicate which channel is used or not
 * @return false if the phy is invalid
 */
bool ism_set_phy(uint8_t phy, const uint8_t* channels);

/**
 * @brief Stop ISM module and restart
 */
void ism_disconnect(void);

/**
 * @brief Set ISM sync mode
 * @param mode new sync mode
 */
void ism_set_sync_mode(ism_sync_mode_t mode);

/**
 * @brief Enable or disable the radio. By default the radio is enable.
 * @param enable true to enable the radio
 */
void ism_enable(bool enable);

/**
 * @brief Set if the beacon server should accept synchronization of unassociated client, use ism_config() to change the associated beacon
 * @param accept true to accept synchronization of unassociated client
 * @return false in case of error
 */
bool ism_set_accept_unassociated(bool accept);

/**
 * @brief Set the ID of the beacon server
 * @param id new ID of the beacon server
 * @return false in case of error
 */
bool ism_set_sync_beacon_id(uint64_t id);

/**
 * @brief Power down the ISM module
 */
void ism_power_down(void);

/**
 * @brief Tick. Poll for new RX data or need for module reconfiguration
 */
void ism_tick(void);

/**
 * @brief TX unicast frame
 * @param destination destination address
 * @param data pointer to data to transmit
 * @param size size of data to transmit (<= ISM_MAX_UNICAST_DATA_SIZE)
 * @return true if frame was sent to module
 */
bool ism_tx_unicast(uint8_t destination, const uint8_t* data, uint8_t size);

/**
 * @brief TX multicast frame
 * @param group groups to broadcast to
 * @param number number of retransmission of the frame
 * @param data pointer to data to transmit
 * @param size size of data to transmit (<= ISM_MAX_MULTICAST_DATA_SIZE)
 * @return true if frame was sent to module
 */
bool ism_tx_multicast(uint32_t group, uint8_t number, const uint8_t* data, uint8_t size);

/**
 * @brief Set the data to add to the sync frame.
 * @param data pointer to data to add to the sync frame
 * @param size size of data (<= ISM_MAX_SYNC_USER_DATA_SIZE)
 * @return false in case of error
 */
bool ism_set_sync_user_data(const uint8_t* data, uint8_t size);

/**
 * @brief Get if there is a a pending radio transmission
 * @return true if there is a a pending radio transmission
 */
bool ism_is_tx_pending(void);

/**
 * @brief Get if the ISM is initialized and there is no pending UART transmission
 * @return true if the ISM is initialized and there is no pending UART transmission
 */
bool ism_is_ready(void);

/**
 * @brief Get the maximum data size of unicast frame
 * @return the maximum data size of unicast frame
 */
uint8_t ism_get_max_unicast_data_size(void);

/**
 * @brief Get the maximum data size of multicast frame
 * @return the maximum data size of multicast frame
 */
uint8_t ism_get_max_multicast_data_size(void);

/**
 * @brief Get the firmware version
 * @return a string containing the firmware version
 */
char* ism_get_firmware_version(void);

/**
 * @brief Get the firmware version coded into an integer.
 * Version a.b.c => a << 16 + b << 8 + c
 * @return the firmware version coded into an integer
 */
uint32_t ism_get_firmware_version_value(void);

/**
 * @brief Get the hardware version
 * @return a string containing the hardware version
 */
char* ism_get_hardware_version(void);

/**
 * @brief Request communication statistics from ISM module
 * @return false if the ISM is not initialized
 */
bool ism_request_stat(void);

/**
 * @brief Get the TDMA state
 * @return the TDMA state
 */
ism_state_t ism_get_state(void);

/**
 * @brief Request TDMA state from ISM module. Only useful for incrementing the rx_counter when the radio module reply so we can check with ism_get_uart_rx_counter() that radio module is still alive.
 * @return false if the ISM is not initialized
 */
bool ism_request_state(void);

/**
 * @brief Get the number of uart frame received
 * @return the number of uart frame received
 */
uint32_t ism_get_uart_rx_counter(void);

/**
 * @brief Get the number of bytes used by the channel plan of a selected phy, the size is used for calling ism_set_phy()
 * @param phy the phy to get the channel plan byte size
 * @return the channel plan byte size
 */
uint8_t ism_get_channels_size(uint8_t phy);

#endif
