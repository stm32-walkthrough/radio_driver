/**
 ******************************************************************************
 * @file    radio_config.h
 * @author  nicolas.brunner@heig-vd.ch
 * @date    14-June-2023
 * @brief   Radio configuration template file.
 *          This file should be copied to the application folder and renamed
 *          to radio_config.h.
 ******************************************************************************
 * @copyright HEIG-VD
 *
 * License information
 *
 ******************************************************************************
 */

#ifndef RADIO_CONFIG_H
#define RADIO_CONFIG_H

/* Includes ------------------------------------------------------------------*/

#include <stdint.h>

#include "stm32l4xx.h"

/* Exported preprocessor constants -------------------------------------------*/

// API configuration
//#define ISM_RAW_STAT ///< Uncomment to use raw statistics instead of a struct

// Protocol configuration
#define ISM_DATA_SLOT_COUNT          9
#define ISM_RETRY_COUNT              2
#define ISM_SYNC_RX_INTERVAL         1
#define ISM_SYNC_RX_MISS_MAX         10
#define ISM_SCAN_FIRST_ON_DURATION   402 ///< milliseconds, max 65535
#define ISM_SCAN_ON_DURATION         202 ///< milliseconds, max 65535
#define ISM_SCAN_OFF_DURATION        400000 ///< milliseconds, max 16777215
#define ISM_PATTERN                  0x3D, 0x47, 0x78, 0x34 ///< Must be 4 bytes separated by comma

// Hardware configuration
#define UNCONNECTED_PIN              {NULL}

#define ISM_TX                       {GPIOC, GPIO_PIN_1}
#define ISM_RX                       {GPIOC, GPIO_PIN_0}
#define ISM_CTS                      {GPIOB, GPIO_PIN_5}
#define ISM_RTS                      {GPIOB, GPIO_PIN_4}
#define ISM_GPIO0                    {GPIOB, GPIO_PIN_11}
#define ISM_GPIO1                    {GPIOB, GPIO_PIN_12}
#define ISM_GPIO2                    {GPIOC, GPIO_PIN_8}
#define ISM_GPIO3                    {GPIOC, GPIO_PIN_9}
#define ISM_BOOT0                    {GPIOC, GPIO_PIN_7}
#define ISM_NRESET                   {GPIOC, GPIO_PIN_6}
#define ISM_NPEN                     {GPIOB, GPIO_PIN_1}

#define ISM_GPIO1_EXTI_IRQn          EXTI15_10_IRQn
#define ISM_GPIO1_IRQHandler         EXTI15_10_IRQHandler

#define ISM_UART_NUMBER              -1
#define ISM_UART                     LPUART1
#define ISM_UART_IRQn                LPUART1_IRQn
#define ISM_UART_IRQHandler          LPUART1_IRQHandler
#define ISM_UART_CLK_ENABLE          __HAL_RCC_LPUART1_CLK_ENABLE
#define ISM_UART_AF                  GPIO_AF8_LPUART1
#define ISM_UART_HWCONTROL           UART_HWCONTROL_NONE

#define ISM_UART_DMA_TX_CHANNEL      DMA2_Channel6
#define ISM_UART_DMA_RX_CHANNEL      DMA2_Channel7
#define ISM_UART_DMA_TX_IRQn         DMA2_Channel6_IRQn
#define ISM_UART_DMA_RX_IRQn         DMA2_Channel7_IRQn
#define ISM_UART_DMA_TX_IRQHandler   DMA2_Channel6_IRQHandler
#define ISM_UART_DMA_RX_IRQHandler   DMA2_Channel7_IRQHandler
#define ISM_UART_DMA_CLK_ENABLE      __HAL_RCC_DMA2_CLK_ENABLE
#define ISM_UART_DMA_REQUEST         DMA_REQUEST_4

#define ISM_UART_BAUDRATE            115200 ///< must be lower than 30000 for using stop2 mode of RM1

#endif
