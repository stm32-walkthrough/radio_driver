# rm1s3_driver
Driver for the RM1S3 radio module that work with a STM32 target.
For additional information, please look at the [example](https://gitlab.com/stm32-walkthrough/radio_example) and the [documentation](https://gitlab.com/stm32-walkthrough/doc/-/blob/main/rm1s3_driver/rm1s3_driver.pdf).

## Install
- `git submodule add https://gitlab.com/stm32-walkthrough/rm1s3_driver Drivers/rm1s3_driver`
- Copy `Drivers/rm1s3_driver/radio_config_template.h` to `Core/Inc/radio_config.h`
- Edit `radio_config.h` to match your hardware
- C/C++ General -> Paths and Symbols -> Includes -> GNU C -> Add... -> `Drivers/rm1s3_driver/Inc`

## Arduino
A port of this driver is available for arduino, it can be found in the [arduino](https://gitlab.com/stm32-walkthrough/rm1s3_driver/-/tree/arduino) branch.
